# Hardware.IC-01.03

## Interfaz de Comandos

Autor: 

- Agustín González 
- Germán Fernández

Revisión: 

- Agustín González 
- Germán Fernández

Hardware:

- 5 salidas optoacopladas a TRIAC
- 5 salidas a RELÉ
- 5 Salidas a drenador abierto.
- Aislación de salidas del PIC
- Conector para segunda placa IComando

Software:

- Eagle 6.5.0 Light

Usos:

- Semáforo
- Tablero de Comandos